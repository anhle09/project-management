import { Outlet, Navigate } from 'react-router-dom'
import { useAuthDataSelector } from 'redux/store'
import { ADMIN_ROLE } from 'shared/constant/userRoles'

export function LoginRequiredRoute() {
  const { currentUser } = useAuthDataSelector()

  if (currentUser.email) {
    return <Outlet />
  }
  return <Navigate to='/login' replace />
}

export function AdminRequiredRoute() {
  const { currentUser } = useAuthDataSelector()

  if (currentUser.role === ADMIN_ROLE) {
    return <Outlet />
  }
  return <Navigate to='/404' replace />
}
