import { Route, Routes, Navigate } from 'react-router-dom'
import { unstable_HistoryRouter as HistoryRouter } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import { AdminRequiredRoute, LoginRequiredRoute } from './ProtectedRoute'
import Login from 'features/Auth/Login'
import Register from 'features/Auth/Register'
import ChangePassword from 'features/Auth/ChangePassword'

import MainLayout from 'shared/components/Layout'
import NotFoundPage from 'shared/components/NotFoundPage'
import Dashboard from 'features/Dashboard'

import ProjectList from 'features/Project/ProjectList'
import ProjectDetail from 'features/Project/ProjectDetail'
import ProjectCreate from 'features/Project/ProjectEdit/ProjectCreate'
import ProjectUpdate from 'features/Project/ProjectEdit/ProjectUpdate'

import TaskList from 'features/Task/TaskList'
import TaskCreate from 'features/Task/TaskEdit/TaskCreate'
import TaskUpdate from 'features/Task/TaskEdit/TaskUpdate'

import UserList from 'features/User/UserList'
import UserCreate from 'features/User/UserEdit/UserCreate'
import UserUpdate from 'features/User/UserEdit/UserUpdate'

export const browserHistory = createBrowserHistory()

export default function Router() {
  return (
    <HistoryRouter history={browserHistory} basename='management-app'>
      <Routes>
        <Route index element={<Navigate to='dashboard' replace />} />
        <Route path='login' element={<Login />} />
        <Route path='register' element={<Register />} />
        <Route path='change-password' element={<ChangePassword />} />

        <Route element={<LoginRequiredRoute />}>
          <Route element={<MainLayout />}>
            <Route path='dashboard' element={<Dashboard />} />
            <Route path='dashboard/:type/:id' element={<Dashboard />} />

            <Route path='projects' element={<ProjectList />} />
            <Route path='projects/detail/:id' element={<ProjectDetail />} />

            <Route element={<AdminRequiredRoute />}>
              <Route path='projects/create' element={<ProjectCreate />} />
              <Route path='projects/edit/:id' element={<ProjectUpdate />} />

              <Route path='users' element={<UserList />}>
                <Route path='create' element={<UserCreate />} />
                <Route path='edit/:id' element={<UserUpdate />} />
              </Route>
            </Route>

            <Route path='tasks' element={<TaskList />}>
              <Route element={<AdminRequiredRoute />}>
                <Route path='create' element={<TaskCreate />} />
              </Route>
              <Route path=':id' element={<TaskUpdate />} />
            </Route>
          </Route>
        </Route>

        <Route path='*' element={<NotFoundPage />} />
      </Routes>
    </HistoryRouter>
  )
}
