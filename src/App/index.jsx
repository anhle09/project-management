import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { authAction } from 'redux/Reducer/authSlice'
import { userAction } from 'redux/Reducer/userSlice'
import { projectAction } from 'redux/Reducer/projectSlice'
import { taskAction } from 'redux/Reducer/taskSlice'
import { useAuthDataSelector } from 'redux/store'
import { ADMIN_ROLE } from 'shared/constant/userRoles'
import LoadingPage from 'shared/components/LoadingPage'
import Router from './Router'

export default function App() {
  const { currentUser, isStarting } = useAuthDataSelector()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(authAction.getCurrentUser())
  }, [dispatch])

  useEffect(() => {
    if (currentUser.email) {
      dispatch(projectAction.queryProject())
      dispatch(taskAction.queryTask())
    }
    if (currentUser.role === ADMIN_ROLE) {
      dispatch(userAction.queryUser())
    }
  }, [currentUser, dispatch])

  if (isStarting) return <LoadingPage />
  return <Router />
}
