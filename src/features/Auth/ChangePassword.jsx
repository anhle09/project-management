import { useEffect } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useAuthDataSelector } from 'redux/store'
import { authAction } from 'redux/Reducer/authSlice'
import { PasswordField } from './Layout'
import LoadingSpinner from 'shared/components/LoadingSpinner'
import Layout from './Layout'

const formValidate = Yup.object({
  password: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
  newPassword: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
})

export default function ChangePassword() {
  const { isLoading, currentUser, errorMessage } = useAuthDataSelector()
  const dispatch = useDispatch()

  useEffect(() => {
    return () => dispatch(authAction.resetError())
  }, [dispatch])

  const handleFormSubmit = (formData) => {
    dispatch(authAction.changePassword(formData))
  }

  const formInit = {
    email: currentUser.email,
    password: '',
    newPassword: '',
  }

  return (
    <Layout>
      <section className='p-7 shadow bg-white rounded-bottom overflow-hidden'>
        <div className='text-center text-secondary'>
          <h4>Change password</h4>
          <p className='px-4'>Enter old password and new password</p>
        </div>

        <Formik
          initialValues={formInit}
          onSubmit={handleFormSubmit}
          validationSchema={formValidate}
        >
          {({ touched, errors }) => (
            <Form className='mt-6'>
              <div className='mb-2'>
                <PasswordField
                  label='Password'
                  name='password'
                  error={touched.password && errors.password}
                />
              </div>

              <div className='mb-2'>
                <PasswordField
                  label='New password'
                  name='newPassword'
                  error={touched.newPassword && errors.newPassword}
                />
              </div>

              {errorMessage && (
                <small className='text-danger'>{errorMessage}</small>
              )}

              <Button type='submit' className='mt-5 mb-3 w-100 text-white'>
                Save <LoadingSpinner active={isLoading} />
              </Button>
            </Form>
          )}
        </Formik>
      </section>
    </Layout>
  )
}
