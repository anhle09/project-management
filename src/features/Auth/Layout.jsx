import { useState } from 'react'
import { Field } from 'formik'
import { EyeIcon, EyeOffIcon } from '@heroicons/react/outline'
import { LOGO_URL } from 'shared/constant/assetUrl'

export default function Layout({ children }) {
  return (
    <div className='d-flex justify-content-center align-items-center min-vh-100'>
      <main style={{ flexBasis: 420 }}>
        <div className='text-center shadow bg-primary rounded-top overflow-hidden'>
          <img src={LOGO_URL} alt='logo' style={{ height: 32, margin: 28 }} />
        </div>
        {children}
      </main>
    </div>
  )
}

export function PasswordField({ label, name, error }) {
  const [showPassword, setShowPassword] = useState(false)
  const toggleShowPassword = () => setShowPassword((prev) => !prev)

  return (
    <label className='d-block'>
      <p className='mb-2'>{label}</p>
      <div className='position-relative'>
        <Field
          name={name}
          type={showPassword ? 'text' : 'password'}
          className={error ? 'form-control form-error' : 'form-control'}
        />
        <span
          className='position-absolute top-0 bottom-0 end-0 pointer input-group-text'
          onClick={toggleShowPassword}
        >
          {showPassword ? (
            <EyeOffIcon className='text-secondary' width={18} />
          ) : (
            <EyeIcon className='text-secondary' width={18} />
          )}
        </span>
      </div>
      {error && <small className='mt-1 text-danger'>{error}</small>}
    </label>
  )
}

export const TextField = ({ type = 'text', label, name, error }) => (
  <label className='d-block'>
    <p className='mb-2'>{label}</p>
    <Field
      name={name}
      type={type}
      className={error ? 'form-control form-error' : 'form-control'}
    />
    {error && <small className='mt-1 text-danger'>{error}</small>}
  </label>
)
