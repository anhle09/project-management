import { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useAuthDataSelector } from 'redux/store'
import { authAction } from 'redux/Reducer/authSlice'
import { TextField, PasswordField } from './Layout'
import LoadingSpinner from 'shared/components/LoadingSpinner'
import Layout from './Layout'

const formValidate = Yup.object({
  email: Yup.string().email().required('Required'),
  password: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
})

export default function Login() {
  const { isLoading, errorMessage } = useAuthDataSelector()
  const dispatch = useDispatch()

  useEffect(() => {
    return () => dispatch(authAction.resetError())
  }, [dispatch])

  const handleLogIn = (formData) => {
    dispatch(authAction.login(formData))
  }

  return (
    <Layout>
      <section className='shadow p-7 bg-white rounded-bottom overflow-hidden'>
        <div className='text-center text-secondary'>
          <h4>Sign In</h4>
          <p className='px-4'>
            Enter your email address and password to access admin panel
          </p>
        </div>

        <Formik
          initialValues={{ email: 'admin@mail.com', password: 'admin' }}
          onSubmit={handleLogIn}
          validationSchema={formValidate}
        >
          {({ touched, errors }) => (
            <Form className='mt-6'>
              <div className='mb-4'>
                <TextField
                  label='Email address'
                  type='email'
                  name='email'
                  error={touched.email && errors.email}
                />
              </div>

              <div className='mb-2'>
                <Link to='#' className='float-end'>
                  <small className='text-secondary'>
                    Forgot your password?
                  </small>
                </Link>
                <PasswordField
                  label='Password'
                  name='password'
                  error={touched.password && errors.password}
                />
              </div>

              {errorMessage && (
                <small className='text-danger'>{errorMessage}</small>
              )}

              <Button type='submit' className='mt-5 mb-3 w-100 text-white'>
                Login <LoadingSpinner active={isLoading} />
              </Button>
            </Form>
          )}
        </Formik>
      </section>

      <div className='mt-4 text-center text-secondary'>
        Don't have an account?{' '}
        <Link to='/register' className='fw-bold'>
          Sign up
        </Link>
      </div>
    </Layout>
  )
}
