import { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import { useAuthDataSelector } from 'redux/store'
import { authAction } from 'redux/Reducer/authSlice'
import { TextField, PasswordField } from './Layout'
import LoadingSpinner from 'shared/components/LoadingSpinner'
import Layout from './Layout'

const formValidate = Yup.object({
  firstName: Yup.string().required('Required'),
  email: Yup.string().email().required('Required'),
  password: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
})

const formInit = {
  firstName: '',
  email: '',
  password: '',
  role: 0,
}

export default function Register() {
  const { isLoading, errorMessage } = useAuthDataSelector()
  const dispatch = useDispatch()

  useEffect(() => {
    return () => dispatch(authAction.resetError())
  }, [dispatch])

  const handleRegister = (formData) => {
    dispatch(authAction.register(formData))
  }

  return (
    <Layout>
      <section className='p-6 shadow bg-white rounded-bottom overflow-hidden'>
        <div className='text-center text-secondary'>
          <h4>Sign Up</h4>
          <p className='px-4'>
            Enter your email address and password to access admin panel
          </p>
        </div>

        <Formik
          initialValues={formInit}
          onSubmit={handleRegister}
          validationSchema={formValidate}
        >
          {({ touched, errors }) => (
            <Form className='mt-5'>
              <div className='mb-4'>
                <TextField
                  label='Name'
                  type='text'
                  name='firstName'
                  error={touched.firstName && errors.firstName}
                />
              </div>

              <div className='mb-4'>
                <TextField
                  label='Email address'
                  type='email'
                  name='email'
                  error={touched.email && errors.email}
                />
              </div>

              <div className='mb-2'>
                <PasswordField
                  label='Password'
                  name='password'
                  error={touched.password && errors.password}
                />
              </div>

              {errorMessage && (
                <small className='text-danger'>{errorMessage}</small>
              )}

              <Button type='submit' className='mt-5 mb-3 w-100 text-white'>
                Register <LoadingSpinner active={isLoading} />
              </Button>
            </Form>
          )}
        </Formik>
      </section>

      <div className='mt-4 text-center text-secondary'>
        Already have an account?{' '}
        <Link to='/login' className='fw-bold'>
          Sign in
        </Link>
      </div>
    </Layout>
  )
}
