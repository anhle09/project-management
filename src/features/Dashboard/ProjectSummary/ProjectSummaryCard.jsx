import { BriefcaseIcon } from '@heroicons/react/outline'
import { Link } from 'react-router-dom'

export default function ProjectSummaryCard({ summaryData }) {
  return (
    <div className='h-100 p-5 rounded shadow-sm bg-white overflow-auto'>
      <h6>PROJECTS SUMMARY</h6>

      <div className='mt-4 text-secondary'>
        <SummaryItem summaryData={summaryData.inprogress}>
          <BriefcaseIcon width={24} className='float-end' />
        </SummaryItem>

        <SummaryItem summaryData={summaryData.nextDays}>
          <BriefcaseIcon width={24} className='float-end' />
        </SummaryItem>

        <SummaryItem summaryData={summaryData.highPriority}>
          <BriefcaseIcon width={24} className='float-end' />
        </SummaryItem>

        <SummaryItem summaryData={summaryData.mediumPriority}>
          <BriefcaseIcon width={24} className='float-end' />
        </SummaryItem>
      </div>
    </div>
  )
}

const SummaryItem = ({ children, summaryData }) => (
  <Link
    to={'/dashboard/project/' + summaryData.key}
    className='d-block mb-2 px-5 py-4 bg-white rounded shadow-sm'
  >
    {children}
    <h6>{summaryData.title}</h6>
    <h3 className='mt-2 mb-0'>{summaryData.projList.length}</h3>
  </Link>
)
