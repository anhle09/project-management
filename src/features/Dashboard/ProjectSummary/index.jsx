import moment from 'moment'
import { Link } from 'react-router-dom'
import { ProjectPriorityLabel } from 'shared/components/StatusLabel'
import classes from './styles.module.scss'

export default function ProjectSummary({ summaryData }) {
  const { title, projList } = summaryData

  return (
    <div className='h-100 p-5 rounded shadow-sm bg-white overflow-auto'>
      <h6>{title.toUpperCase()}</h6>

      {projList.length ? (
        <table className='table text-secondary mt-2'>
          <tbody className={classes.summary_table}>
            {projList.map((item) => (
              <ProjectRow key={item.id} projData={item} />
            ))}
          </tbody>
        </table>
      ) : (
        <small className='mt-4'>Project list is empty</small>
      )}
    </div>
  )
}

const ProjectRow = ({ projData }) => (
  <tr>
    <td>
      <Link to={`/projects/detail/${projData.id}`}>
        <b>{projData.name}</b>
      </Link>
      <small>$ {projData.budget.toLocaleString()}</small>
    </td>
    <td>
      <small>Priority</small>
      <ProjectPriorityLabel priority={projData.priority} />
    </td>
    <td>
      <small>Start Date</small>
      <div>{moment(projData.startDate).format('LL')}</div>
    </td>
    <td>
      <small>End Date</small>
      <div>{moment(projData.endDate).format('LL')}</div>
    </td>
  </tr>
)
