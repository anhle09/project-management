import { Chart, ArcElement, Tooltip } from 'chart.js'
import { Doughnut } from 'react-chartjs-2'
import { TrendingDownIcon, TrendingUpIcon } from '@heroicons/react/outline'
import classes from '../styles.module.scss'

Chart.register(ArcElement, Tooltip)

const colors = ['#0acf97', '#727cf5', '#fa5c7c']

const calculatePercent = (item, total) => {
  const percent = (item / total) * 100
  return Math.round(percent)
}

export default function StatusChart({ taskStatus }) {
  const { label, data, total } = taskStatus
  if (total === 0)
    return (
      <div className='h-100 p-5 rounded shadow-sm bg-white'>
        <h6>TASKS STATUS</h6>
        <p>Task list is empty</p>
      </div>
    )

  const chartData = {
    labels: label,
    datasets: [
      {
        data: data,
        backgroundColor: colors,
        borderWidth: 0,
      },
    ],
  }

  const completedPercent = calculatePercent(data[0], total)
  const inprogressPercent = calculatePercent(data[1], total)
  const pendingPercent = 100 - completedPercent - inprogressPercent

  return (
    <div className='h-100 p-5 rounded shadow-sm bg-white'>
      <h6>TASKS STATUS</h6>

      <div className={classes.chart_container}>
        <Doughnut data={chartData} options={{ cutout: '80%' }} height={200} />
      </div>

      <div className='row text-center text-secondary'>
        <div className='col'>
          <TrendingUpIcon width={24} color={colors[0]} />
          <h5 className='mt-2'>{completedPercent}%</h5>
          <small>{label[0]}</small>
        </div>

        <div className='col'>
          <TrendingDownIcon width={24} color={colors[1]} />
          <h5 className='mt-2'>{inprogressPercent}%</h5>
          <small>{label[1]}</small>
        </div>

        <div className='col'>
          <TrendingDownIcon width={24} color={colors[2]} />
          <h5 className='mt-2'>{pendingPercent}%</h5>
          <small>{label[2]}</small>
        </div>
      </div>
    </div>
  )
}
