import moment from 'moment'
import { Link } from 'react-router-dom'
import {
  useAuthDataSelector,
  useProjectDataSelector,
  useUserDataSelector,
} from 'redux/store'
import { TaskStatusLabel } from 'shared/components/StatusLabel'
import getById from 'shared/utils/getById'

export default function TaskItem({ taskData }) {
  const { currentUser } = useAuthDataSelector()
  const { userList } = useUserDataSelector()
  const { projectList } = useProjectDataSelector()
  const projData = getById(projectList, taskData?.project)
  const userData = currentUser.role
    ? getById(userList, taskData?.member)
    : currentUser

  return (
    <tr>
      <td>
        <Link to={`/tasks/${taskData?.id}`}>
          <b>{taskData.name}</b>
        </Link>
        <small>{projData?.name}</small>
      </td>
      <td>
        <small>Status</small>
        <TaskStatusLabel status={taskData.status} />
      </td>
      <td>
        <small>Assigned to</small>
        <div>{userData?.firstName}</div>
      </td>
      <td>
        <small>End time</small>
        <div>{moment(taskData.endDate).format('LL')}</div>
      </td>
    </tr>
  )
}
