import { Link } from 'react-router-dom'
import { BriefcaseIcon } from '@heroicons/react/outline'
import classes from './styles.module.scss'

export default function TaskSummaryCard({ summaryData }) {
  const { outdated, pending, inprogress, nextDays } = summaryData

  return (
    <div className='shadow-sm rounded overflow-hidden'>
      <div className={classes.summary}>
        <SummaryItem summaryData={outdated}>
          <BriefcaseIcon width={24} />
        </SummaryItem>

        <SummaryItem summaryData={pending}>
          <BriefcaseIcon width={24} />
        </SummaryItem>

        <SummaryItem summaryData={inprogress}>
          <BriefcaseIcon width={24} />
        </SummaryItem>

        <SummaryItem summaryData={nextDays}>
          <BriefcaseIcon width={24} />
        </SummaryItem>
      </div>
    </div>
  )
}

const SummaryItem = ({ children, summaryData }) => (
  <Link to={'/dashboard/task/' + summaryData.key}>
    {children}
    <h3 className='my-1'>{summaryData.taskList.length}</h3>
    <small>{summaryData.title}</small>
  </Link>
)
