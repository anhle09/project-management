import TaskItem from './TaskItem'
import classes from './styles.module.scss'

export default function TaskSummary({ summaryData }) {
  const { title, taskList } = summaryData

  return (
    <div className='h-100 p-5 rounded shadow-sm bg-white overflow-auto'>
      <h6>{title.toUpperCase()}</h6>

      {taskList.length ? (
        <table className='table text-secondary mt-2'>
          <tbody className={classes.summary_table}>
            {taskList.map((item) => (
              <TaskItem key={item.id} taskData={item} />
            ))}
          </tbody>
        </table>
      ) : (
        <small className='mt-4'>Task list is empty</small>
      )}
    </div>
  )
}
