import moment from 'moment'
import { useParams } from 'react-router-dom'
import { useProjectDataSelector, useTaskDataSelector } from 'redux/store'
import StatusChart from './StatusChart'
import TaskSummaryCard from './TaskSummary/TaskSummaryCard'
import ProjectSummaryCard from './ProjectSummary/ProjectSummaryCard'
import ProjectSummary from './ProjectSummary'
import TaskSummary from './TaskSummary'
import classes from './styles.module.scss'
import {
  HIGH_PRIORITY,
  MEDIUM_PRIORITY,
  TASK_COMPLETED,
  TASK_IN_PROGRESS,
  TASK_PENDING,
} from 'shared/constant/projectStatus'

export default function Dashboard() {
  const { type, id } = useParams()
  const { taskList } = useTaskDataSelector()
  const { projectList } = useProjectDataSelector()

  const { taskSummary, taskStatus } = taskFilter(taskList)
  const projectSummary = projectFilter(projectList)

  const TableSummary = () => {
    switch (type) {
      case 'project':
        return <ProjectSummary summaryData={projectSummary[id]} />
      case 'task':
        return <TaskSummary summaryData={taskSummary[id]} />
      default:
        return <TaskSummary summaryData={taskSummary.pending} />
    }
  }

  return (
    <>
      <section className='mt-4'>
        <TaskSummaryCard summaryData={taskSummary} />
      </section>

      <div className={classes.dashboard_container}>
        <section className='col-3'>
          <StatusChart taskStatus={taskStatus} />
        </section>
        <section className='col px-4'>
          <TableSummary />
        </section>
        <section className='col-3'>
          <ProjectSummaryCard summaryData={projectSummary} />
        </section>
      </div>
    </>
  )
}

const now = moment().format('YYYY-MM-DD')

const taskFilter = (taskList) => {
  const taskPending = taskList.filter((item) => item.status === TASK_PENDING)
  const taskInprogress = taskList.filter(
    (item) => item.status === TASK_IN_PROGRESS
  )
  const taskCompleted = taskList.filter(
    (item) => item.status === TASK_COMPLETED
  )

  const taskStatus = {
    label: ['Completed', 'In-progress', 'Pending'],
    data: [taskCompleted.length, taskInprogress.length, taskPending.length],
    total: taskList.length,
  }

  const taskSummary = {
    outdated: {
      key: 'outdated',
      title: 'Outdated Task',
      taskList: taskList.filter(
        (item) => item.status !== TASK_COMPLETED && item.endDate < now
      ),
    },
    pending: {
      key: 'pending',
      title: 'Pending Task',
      taskList: taskPending,
    },
    inprogress: {
      key: 'inprogress',
      title: 'In-progress Task',
      taskList: taskInprogress,
    },
    nextDays: {
      key: 'nextDays',
      title: 'Next 3 Days Task',
      taskList: taskList.filter((item) => {
        const diffDays = moment(item.endDate).diff(now, 'days')
        return diffDays > 0 && diffDays <= 3
      }),
    },
  }
  return { taskStatus, taskSummary }
}

const projectFilter = (projectList) => {
  const projectSummary = {
    inprogress: {
      key: 'inprogress',
      title: 'In-progress Project',
      projList: projectList.filter(
        (item) => item.startDate < now && item.endDate > now
      ),
    },
    highPriority: {
      key: 'highPriority',
      title: 'High Priority Project',
      projList: projectList.filter((item) => item.priority === HIGH_PRIORITY),
    },
    mediumPriority: {
      key: 'mediumPriority',
      title: 'Medium Priority Project',
      projList: projectList.filter((item) => item.priority === MEDIUM_PRIORITY),
    },
    nextDays: {
      key: 'nextDays',
      title: 'Released in the next 7 days',
      projList: projectList.filter((item) => {
        const diffDays = moment(item.endDate).diff(now, 'days')
        return diffDays > 0 && diffDays <= 7
      }),
    },
  }
  return projectSummary
}
