import { TrashIcon } from '@heroicons/react/outline'
import { Badge } from 'react-bootstrap'
import { roles } from 'shared/constant/userRoles'

export default function MemberItem({ userData, removeMember }) {
  return (
    <tr>
      <td>
        {userData.lastName} {userData.firstName}
      </td>
      <td>{userData.email}</td>
      <td>{roles[userData.role]}</td>
      <td>
        <Badge bg='success' className='text-white text-xs'>
          Active
        </Badge>
      </td>
      <td>
        <TrashIcon
          width={20}
          className='ms-2 text-danger pointer'
          onClick={() => removeMember(userData.id)}
        />
      </td>
    </tr>
  )
}
