import { useState } from 'react'
import { Dropdown } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import { projectAction } from 'redux/Reducer/projectSlice'
import { useUserDataSelector } from 'redux/store'
import projectApi from 'shared/api/projectApi'

export default function MemberSearch({ memberIdList }) {
  const { id } = useParams()
  const { userList } = useUserDataSelector()
  const [searchResult, setSearchResult] = useState(null)
  const [menuShow, setMenuShow] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const dispatch = useDispatch()

  const handleAddMember = async (memberId) => {
    const memberList = [...memberIdList]
    memberList.push(memberId)
    await projectApi.update(id, { members: memberList })
    dispatch(projectAction.queryProject())
    toggleMenuShow()
  }

  const toggleMenuShow = () => {
    setMenuShow(false)
    setInputValue('')
  }

  const handleSearch = (event) => {
    const inputText = event.target.value
    if (!inputText.length) toggleMenuShow()
    const text = inputText?.toLowerCase().trim()
    if (text < 1) return

    setInputValue(inputText?.trim())
    const users = userList.filter((item) => {
      const fullName = item.lastName + ' ' + item.firstName
      return fullName?.toLowerCase().includes(text)
    })

    if (users.length) {
      setSearchResult(users)
    } else {
      setSearchResult(null)
    }
    if (!menuShow) setMenuShow(true)
  }

  return (
    <div className='mt-2 d-inline-block'>
      <input
        className='form-control'
        style={{ height: 32, width: 280 }}
        placeholder='Add member'
        value={inputValue}
        onChange={handleSearch}
      />

      <Dropdown show={menuShow} onToggle={toggleMenuShow}>
        <Dropdown.Menu className='w-100 mt-2 shadow'>
          <div style={{ maxHeight: 200, overflow: 'auto' }}>
            {searchResult?.map((item) => (
              <Dropdown.Item
                key={item.id}
                as='div'
                className='d-flex justify-content-between align-items-center pointer'
                onClick={() => handleAddMember(item.id)}
              >
                {item.lastName} {item.firstName}
              </Dropdown.Item>
            ))}
            {!searchResult && <Dropdown.Item as='div'>No result</Dropdown.Item>}
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  )
}
