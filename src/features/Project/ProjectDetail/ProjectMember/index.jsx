import { useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { useUserDataSelector } from 'redux/store'
import { projectAction } from 'redux/Reducer/projectSlice'
import removeByValue from 'shared/utils/removeByValue'
import projectApi from 'shared/api/projectApi'
import MemberSearch from './MemberSearch'
import MemberItem from './MemberItem'

export default function ProjectMember({ memberIdList }) {
  const dispatch = useDispatch()
  const { id } = useParams()
  const { userList } = useUserDataSelector()
  const memberList = userList.filter((item) => memberIdList?.includes(item.id))

  const removeMember = async (memberId) => {
    const memberList = removeByValue(memberIdList, memberId)
    await projectApi.update(id, { members: memberList })
    dispatch(projectAction.queryProject())
  }

  return (
    <div className='mt-6'>
      <h5>Project Members</h5>
      <MemberSearch memberIdList={memberIdList} />

      {memberIdList?.length ? (
        <table className='table align-middle text-secondary mt-2 mb-0'>
          <thead>
            <tr className='text-sm'>
              <th>Member Name</th>
              <th>Email Address</th>
              <th>Role</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {memberList.map((item) => (
              <MemberItem
                key={item.id}
                userData={item}
                removeMember={removeMember}
              />
            ))}
          </tbody>
        </table>
      ) : (
        <p className='mt-2'>Project members is empty</p>
      )}
    </div>
  )
}
