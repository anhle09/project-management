import moment from 'moment'
import { Badge } from 'react-bootstrap'
import { useParams } from 'react-router-dom'
import { useProjectDataSelector } from 'redux/store'
import { projectStatus } from 'shared/constant/projectStatus'
import ProjectMember from './ProjectMember'
import getById from 'shared/utils/getById'
import LoadingPage from 'shared/components/LoadingPage'
import { AdminRoleRequired } from 'shared/components/ProtectedRole'

export default function ProjectDetail() {
  const { id } = useParams()
  const { projectList } = useProjectDataSelector()
  const projData = getById(projectList, id)
  if (!projData) return <LoadingPage />

  return (
    <div className='m-4 py-6 px-8 rounded shadow-sm bg-white'>
      <h4>{projData.name}</h4>
      <Badge
        bg={projData.priority ? 'danger' : 'warning'}
        className='mt-2 text-sm'
      >
        {projData.priority ? 'High' : 'Medium'}
      </Badge>

      <div className='mt-5'>
        <h5>Project Overview:</h5>
        <div>{projData.description}</div>
      </div>

      <div className='my-5 d-flex'>
        <div className='me-8'>
          <h5>Start date</h5>
          <div>{moment(projData.startDate).format('LL')}</div>
        </div>
        <div className='mx-8'>
          <h5>End date</h5>
          <div>{moment(projData.endDate).format('LL')}</div>
        </div>
        <div className='mx-8'>
          <h5>Budget</h5>
          <div>${projData.budget.toLocaleString()}</div>
        </div>
        <div className='ms-8'>
          <h5>Status</h5>
          <div>{projectStatus[projData.status]}</div>
        </div>
      </div>

      <AdminRoleRequired>
        <ProjectMember memberIdList={projData.members} />
      </AdminRoleRequired>
    </div>
  )
}
