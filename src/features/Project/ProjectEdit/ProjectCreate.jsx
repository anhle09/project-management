import moment from 'moment'
import { projectAction } from 'redux/Reducer/projectSlice'
import { useDispatch } from 'react-redux'
import { PROJECT_API_URL } from 'shared/constant/apiUrl'
import useApi from 'shared/hook/api'
import ProjectForm from './ProjectForm'

const formInit = {
  name: '',
  description: '',
  budget: 0,
  priority: 0,
  status: 0,
  startDate: moment().format('YYYY-MM-DD'),
  endDate: moment().format('YYYY-MM-DD'),
}

export default function ProjectCreate() {
  const [response, postRequest] = useApi.post(PROJECT_API_URL)
  const dispatch = useDispatch()

  const handleSubmit = async (formValues) => {
    await postRequest(formValues)
    dispatch(projectAction.queryProject())
  }

  return (
    <div className='mx-4'>
      <h4 className='mb-5'>Create Project</h4>
      <ProjectForm
        formInit={formInit}
        formSubmit={handleSubmit}
        apiStatus={response.status}
      />
    </div>
  )
}
