import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import {
  ApiStatus,
  FieldDate,
  FieldSelect,
  FieldText,
  FieldTextarea,
  SubmitButton,
} from 'shared/components/FormField'

const formValidate = Yup.object({
  name: Yup.string().required('Required').min(4, 'Too short'),
  description: Yup.string().required('Required').min(4, 'Too short'),
})

export default function ProjectForm({ formInit, formSubmit, apiStatus }) {
  return (
    <div className='p-5 rounded shadow-sm bg-white'>
      <Formik
        enableReinitialize
        initialValues={formInit}
        onSubmit={formSubmit}
        validationSchema={formValidate}
      >
        {({ touched, errors }) => (
          <Form>
            <div className='row'>
              <section className='col col-md-6'>
                <FieldText
                  label='Name'
                  name='name'
                  error={errors.name}
                  touched={touched.name}
                />

                <div className='mt-5'>
                  <FieldTextarea
                    label='Overview'
                    name='description'
                    error={errors.description}
                    touched={touched.description}
                  />
                </div>
              </section>

              <section className='col mt-5 col-md-6 mt-md-0'>
                <FieldText label='Budget' type='number' name='budget' />

                <div className='mt-5'>
                  <FieldSelect label='Priority' name='priority'>
                    <option value='0'>Medium</option>
                    <option value='1'>High</option>
                  </FieldSelect>
                </div>

                <div className='mt-5'>
                  <FieldSelect label='Status' name='status'>
                    <option value='0'>Open</option>
                    <option value='1'>Closed</option>
                  </FieldSelect>
                </div>
              </section>
            </div>

            <div className='row mt-5'>
              <div className='col col-md-6'>
                <FieldDate label='Start Date' name='startDate' />
              </div>

              <div className='col col-md-6'>
                <FieldDate label='End Date' name='endDate' />
              </div>
            </div>

            <div className='mt-5 d-flex align-items-center'>
              <SubmitButton apiStatus={apiStatus} />
              <div className='ms-4'>
                <ApiStatus apiStatus={apiStatus} />
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  )
}
