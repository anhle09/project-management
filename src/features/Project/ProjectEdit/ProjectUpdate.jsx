import { useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { useProjectDataSelector } from 'redux/store'
import { projectAction } from 'redux/Reducer/projectSlice'
import { PROJECT_API_URL } from 'shared/constant/apiUrl'
import useApi from 'shared/hook/api'
import getById from 'shared/utils/getById'
import LoadingPage from 'shared/components/LoadingPage'
import ProjectForm from './ProjectForm'

export default function ProjectUpdate() {
  const dispatch = useDispatch()
  const { id } = useParams()
  const { projectList } = useProjectDataSelector()
  const [response, putRequest] = useApi.patch(PROJECT_API_URL, id)

  const projData = getById(projectList, id)
  if (!projData) return <LoadingPage />

  const handleSubmit = async (formValues) => {
    await putRequest(formValues)
    dispatch(projectAction.queryProject())
  }

  return (
    <div className='mx-4'>
      <h4 className='mb-5'>Update Project</h4>
      <ProjectForm
        formInit={projData}
        formSubmit={handleSubmit}
        apiStatus={response.status}
      />
    </div>
  )
}
