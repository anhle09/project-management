import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { Dropdown } from 'react-bootstrap'
import { projectAction } from 'redux/Reducer/projectSlice'
import projectApi from 'shared/api/projectApi'
import {
  DotsVerticalIcon,
  PencilIcon,
  TrashIcon,
} from '@heroicons/react/outline'

export default function EditMenu({ projectId }) {
  const dispatch = useDispatch()

  const handleDelete = async () => {
    await projectApi.delete(projectId)
    dispatch(projectAction.queryProject())
  }

  return (
    <Dropdown>
      <Dropdown.Toggle as='span' className='float-end'>
        <DotsVerticalIcon width={16} className='btn-icon' />
      </Dropdown.Toggle>

      <Dropdown.Menu className='shadow-sm'>
        <Dropdown.Item as='div' className='pointer'>
          <Link to={`edit/${projectId}`} className='d-flex align-items-center'>
            <PencilIcon width={16} color='gray' />
            <small className='ms-3'>Edit</small>
          </Link>
        </Dropdown.Item>

        <Dropdown.Item as='div' className='pointer'>
          <div onClick={handleDelete} className='d-flex align-items-center'>
            <TrashIcon width={16} color='gray' />
            <small className='ms-3'>Delete</small>
          </div>
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  )
}
