import moment from 'moment'
import { Link } from 'react-router-dom'
import { Badge, ProgressBar } from 'react-bootstrap'
import { AdminRoleRequired } from 'shared/components/ProtectedRole'
import EditMenu from './EditMenu'
import classes from '../styles.module.scss'
import calculateProgress from 'shared/utils/calculateProgress'

export default function ProjectCard({ projData }) {
  const { startDate, endDate } = projData
  const progress = calculateProgress(startDate, endDate)

  return (
    <section className='p-5 shadow-sm bg-white rounded overflow-hidden'>
      <AdminRoleRequired>
        <EditMenu projectId={projData.id} />
      </AdminRoleRequired>

      <Link to={'detail/' + projData.id}>
        <h5>{projData.name}</h5>

        <Badge bg={projData.priority ? 'danger' : 'warning'}>
          {projData.priority ? 'High' : 'Medium'}
        </Badge>

        <div className='mt-3'>
          <h6>Project Overview:</h6>
          <small className={classes.overview}>{projData.description}</small>
        </div>

        <div className='my-3'>
          <div className='me-4 float-end'>
            <h6>End date</h6>
            <div>{moment(endDate).format('LL')}</div>
          </div>
          <h6>Start date</h6>
          <div>{moment(startDate).format('LL')}</div>
        </div>

        <h6 className='mb-3'>
          Progress <span className='float-end'>{progress}%</span>
        </h6>
        <ProgressBar now={progress} style={{ height: 6 }} />
      </Link>
    </section>
  )
}
