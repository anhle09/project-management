import { Link, Outlet } from 'react-router-dom'
import { useProjectDataSelector } from 'redux/store'
import { AdminRoleRequired } from 'shared/components/ProtectedRole'
import ProjectCard from './ProjectCard'
import classes from './styles.module.scss'

export default function ProjectList() {
  const { projectList } = useProjectDataSelector()

  return (
    <div className='px-4'>
      <Outlet />
      <div className='mb-5 d-flex align-items-center'>
        <h4 className='mb-0'>Project list</h4>
        <AdminRoleRequired>
          <Link
            to='create'
            className='ms-5 btn btn-sm btn-danger text-white rounded-pill shadow'
          >
            Create Project
          </Link>
        </AdminRoleRequired>
      </div>
      {!projectList.length && <p>Project list is empty</p>}

      <div className={classes.grid_list}>
        {projectList.map((item) => (
          <ProjectCard key={item.id} projData={item} />
        ))}
      </div>
    </div>
  )
}
