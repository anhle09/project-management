import moment from 'moment'
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { taskAction } from 'redux/Reducer/taskSlice'
import { TASK_API_URL } from 'shared/constant/apiUrl'
import useApi from 'shared/hook/api'
import TaskForm from './TaskForm'

const formInit = {
  project: '',
  member: '',
  name: '',
  description: '',
  status: 0,
  startDate: moment().format('YYYY-MM-DD'),
  endDate: moment().format('YYYY-MM-DD'),
}

export default function TaskCreate() {
  const [response, postRequest] = useApi.post(TASK_API_URL)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleModalHide = () => navigate('/tasks')

  const handleSubmit = async (formValues) => {
    const res = await postRequest(formValues)
    dispatch(taskAction.queryTask())
    if (res) handleModalHide()
  }

  return (
    <Modal show onHide={handleModalHide} size='lg' centered>
      <Modal.Header closeButton>
        <Modal.Title>Create New Task</Modal.Title>
      </Modal.Header>

      <section className='p-6'>
        <TaskForm
          formInit={formInit}
          formSubmit={handleSubmit}
          apiStatus={response.status}
        />
      </section>
    </Modal>
  )
}
