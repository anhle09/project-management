import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useProjectDataSelector, useUserDataSelector } from 'redux/store'
import {
  ApiStatus,
  FieldDate,
  FieldSelect,
  FieldText,
  FieldTextarea,
  SubmitButton,
} from 'shared/components/FormField'

const formValidate = Yup.object({
  project: Yup.string().required('Required'),
  member: Yup.string().required('Required'),
  name: Yup.string().required('Required'),
  description: Yup.string().required('Required'),
  status: Yup.number().required('Required'),
  startDate: Yup.string().required('Required'),
  endDate: Yup.string().required('Required'),
})

export default function TaskForm({ formInit, formSubmit, apiStatus, disabled = false }) {
  const { projectList } = useProjectDataSelector()
  const { userList } = useUserDataSelector()

  return (
    <Formik
      enableReinitialize
      initialValues={formInit}
      onSubmit={formSubmit}
      validationSchema={formValidate}
    >
      {({ touched, errors }) => (
        <Form>
          <fieldset disabled={disabled}>
            <FieldSelect
              label='Project'
              name='project'
              error={errors.project}
              touched={touched.project}
            >
              {projectList.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
            </FieldSelect>

            <div className='row mt-5'>
              <div className='col col-md-8'>
                <FieldText
                  label='Title'
                  name='name'
                  error={errors.name}
                  touched={touched.name}
                />
              </div>

              <div className='col mt-5 col-md-4 mt-md-0'>
                <FieldSelect
                  label='Assign To'
                  name='member'
                  error={errors.member}
                  touched={touched.member}
                >
                  {userList.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.firstName}
                    </option>
                  ))}
                </FieldSelect>
              </div>
            </div>

            <div className='mt-5'>
              <FieldTextarea
                rows={3}
                label='Description'
                name='description'
                error={errors.description}
                touched={touched.description}
              />
            </div>

            <div className='row mt-5'>
              <div className='col col-md-6'>
                <FieldDate label='Start Date' name='startDate' />
              </div>

              <div className='col col-md-6'>
                <FieldDate label='End Date' name='endDate' />
              </div>
            </div>

            {!disabled && (
              <div className='mt-5 d-flex align-items-center'>
                <SubmitButton apiStatus={apiStatus} />
                <div className='ms-4'>
                  <ApiStatus apiStatus={apiStatus} />
                </div>
              </div>
            )}
          </fieldset>
        </Form>
      )}
    </Formik>
  )
}
