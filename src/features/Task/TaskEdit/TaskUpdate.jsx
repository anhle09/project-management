import { useEffect, useState } from 'react'
import { Navigate, useNavigate, useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { useAuthDataSelector, useTaskDataSelector } from 'redux/store'
import { taskAction } from 'redux/Reducer/taskSlice'
import { TASK_API_URL } from 'shared/constant/apiUrl'
import useApi from 'shared/hook/api'
import getById from 'shared/utils/getById'
import TaskForm from './TaskForm'
import { TASK_PENDING } from 'shared/constant/projectStatus'
import { USER_ROLE } from 'shared/constant/userRoles'

export default function TaskUpdate() {
  const { id } = useParams()
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [formDisabled, setFormDisabled] = useState(false)
  const [response, putRequest] = useApi.patch(TASK_API_URL, id)
  const { currentUser } = useAuthDataSelector()
  const { taskList } = useTaskDataSelector()
  const taskData = getById(taskList, id)

  useEffect(() => {
    if (taskData?.status !== TASK_PENDING || currentUser.role === USER_ROLE) {
      setFormDisabled(true)
    }
  }, [currentUser.role, taskData?.status])

  if (!taskData) return <Navigate replace to={'/tasks'} />
  const handleModalHide = () => navigate('/tasks')

  const handleSubmit = async (formValues) => {
    const res = await putRequest(formValues)
    dispatch(taskAction.queryTask())
    if (res) handleModalHide()
  }

  return (
    <Modal show onHide={handleModalHide} size='lg' centered>
      <Modal.Header closeButton>
        <Modal.Title>
          {formDisabled ? 'Task Details' : 'Update Task'}
        </Modal.Title>
      </Modal.Header>

      <section className='p-6 pb-8'>
        <TaskForm
          formInit={taskData}
          formSubmit={handleSubmit}
          apiStatus={response.status}
          disabled={formDisabled}
        />
      </section>
    </Modal>
  )
}
