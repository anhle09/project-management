import { PencilAltIcon, TrashIcon } from '@heroicons/react/outline'
import { taskAction } from 'redux/Reducer/taskSlice'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { TASK_COMPLETED, TASK_PENDING } from 'shared/constant/projectStatus'
import taskApi from 'shared/api/taskApi'

export default function AdminAction({ taskData }) {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const handleEdit = () => navigate(taskData.id)
  const handleDelete = async () => {
    await taskApi.delete(taskData.id)
    dispatch(taskAction.queryTask())
  }

  return (
    <>
      {taskData.status !== TASK_COMPLETED && (
        <TrashIcon
          width={20}
          className='me-2 text-danger pointer'
          onClick={handleDelete}
        />
      )}
      {taskData.status === TASK_PENDING && (
        <PencilAltIcon
          width={20}
          className='text-primary pointer'
          onClick={handleEdit}
        />
      )}
    </>
  )
}
