import { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { taskAction } from 'redux/Reducer/taskSlice'
import { TASK_API_URL } from 'shared/constant/apiUrl'
import useApi from 'shared/hook/api'
import LoadingSpinner from 'shared/components/LoadingSpinner'

export default function UserAction({ taskData }) {
  const [updateButton, setUpdateButton] = useState(null)
  const [response, putRequest] = useApi.patch(TASK_API_URL, taskData.id)
  const dispatch = useDispatch()

  useEffect(() => {
    switch (taskData.status) {
      case 0:
        setUpdateButton({ text: 'Start', variant: 'danger' })
        break
      case 1:
        setUpdateButton({ text: 'Complete', variant: 'success' })
        break
      default:
        setUpdateButton(null)
    }
  }, [taskData.status])

  const handleUpdate = async () => {
    const data = { ...taskData }
    switch (taskData.status) {
      case 0:
        data.status = 1
        break
      default:
        data.status = 2
    }
    await putRequest(data)
    dispatch(taskAction.queryTask())
  }

  if (!updateButton) return null

  return (
    <Button
      size='sm'
      className='text-white'
      variant={updateButton?.variant}
      onClick={handleUpdate}
    >
      {updateButton?.text} <LoadingSpinner active={response.status.loading} />
    </Button>
  )
}
