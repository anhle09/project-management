import moment from 'moment'
import { Link } from 'react-router-dom'
import {
  useAuthDataSelector,
  useProjectDataSelector,
  useUserDataSelector,
} from 'redux/store'
import {
  AdminRoleRequired,
  UserRoleRequired,
} from 'shared/components/ProtectedRole'
import { TaskStatusLabel } from 'shared/components/StatusLabel'
import getById from 'shared/utils/getById'
import AdminAction from './AdminAction'
import UserAction from './UserAction'
import classes from './styles.module.scss'

export default function TaskItem({ taskData }) {
  const { currentUser } = useAuthDataSelector()
  const { userList } = useUserDataSelector()
  const { projectList } = useProjectDataSelector()
  const projData = getById(projectList, taskData?.project)
  const userData = currentUser.role
    ? getById(userList, taskData?.member)
    : currentUser

  return (
    <tr className={classes.task_row}>
      <td>
        <Link to={taskData.id}>
          <b>{taskData.name}</b>
        </Link>
        <small>{projData?.name}</small>
      </td>
      <td>
        <small>Status</small>
        <TaskStatusLabel status={taskData.status} />
      </td>
      <td>
        <small>Assigned to</small>
        <div>{userData?.firstName}</div>
      </td>
      <td>
        <small>Start time</small>
        <div>{moment(taskData.startDate).format('LL')}</div>
      </td>
      <td>
        <small>End time</small>
        <div>{moment(taskData.endDate).format('LL')}</div>
      </td>
      <td>
        <AdminRoleRequired>
          <AdminAction taskData={taskData} />
        </AdminRoleRequired>
        <UserRoleRequired>
          <UserAction taskData={taskData} />
        </UserRoleRequired>
      </td>
    </tr>
  )
}
