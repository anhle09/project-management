import { useState } from 'react'
import { Link, Outlet } from 'react-router-dom'
import { useProjectDataSelector, useTaskDataSelector } from 'redux/store'
import { AdminRoleRequired } from 'shared/components/ProtectedRole'
import TaskItem from './TaskItem'

export default function TaskList() {
  const { projectList } = useProjectDataSelector()
  const { taskList } = useTaskDataSelector()
  const [selectedProj, setSelectedProj] = useState('')

  const taskFilter = selectedProj
    ? taskList.filter((item) => item.project === selectedProj)
    : taskList

  return (
    <div className='px-6'>
      <Outlet />

      <div className='float-end'>
        <select
          className='form-select'
          onChange={(e) => setSelectedProj(e.target.value)}
        >
          <option value=''>All project</option>
          {projectList.map((item) => (
            <option key={item.id} value={item.id}>
              {item.name}
            </option>
          ))}
        </select>
      </div>

      <div className='mb-5 d-flex align-items-center'>
        <h4 className='mb-0 me-5'>Project Tasks</h4>
        <AdminRoleRequired>
          <Link
            to='create'
            className='btn btn-primary btn-sm text-white shadow'
          >
            Add new
          </Link>
        </AdminRoleRequired>
      </div>

      {taskFilter.length ? (
        <div className='px-5 rounded shadow-sm bg-white overflow-auto'>
          <table className='table align-middle text-secondary mt-2 mb-0'>
            <thead>
              <tr className='text-sm'>
                <th>Title</th>
                <th>Status</th>
                <th>Assigned to</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {taskFilter.map((item) => (
                <TaskItem key={item.id} taskData={item} />
              ))}
            </tbody>
          </table>
        </div>
      ) : (
        <p>Task list is empty</p>
      )}
    </div>
  )
}
