import { useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { USER_API_URL } from 'shared/constant/apiUrl'
import useApi from 'shared/hook/api'
import UserForm from './UserForm'
import { useNavigate } from 'react-router-dom'
import { userAction } from 'redux/Reducer/userSlice'

const formInit = {
  firstName: '',
  email: '',
  password: '',
  role: 0,
}

export default function UserCreate() {
  const [response, postRequest] = useApi.post(USER_API_URL)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleModalHide = () => {
    navigate('/users')
  }

  const handleSubmit = async (formValues) => {
    const res = await postRequest(formValues)
    dispatch(userAction.queryUser())
    if (res) handleModalHide()
  }

  return (
    <Modal show onHide={handleModalHide} size='lg' centered>
      <Modal.Header closeButton>
        <Modal.Title>Create New User</Modal.Title>
      </Modal.Header>

      <section className='p-6'>
        <UserForm
          passwordField
          formInit={formInit}
          formSubmit={handleSubmit}
          apiStatus={response.status}
        />
      </section>
    </Modal>
  )
}
