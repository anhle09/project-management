import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import {
  ApiStatus,
  FieldSelect,
  FieldText,
  SubmitButton,
} from 'shared/components/FormField'

export default function UserForm({
  formInit,
  formSubmit,
  apiStatus,
  passwordField = false,
}) {
  const formValidate = Yup.object({
    firstName: Yup.string().required('Required'),
    role: Yup.number().required('Required'),
    email: Yup.string().email().required('Required'),
    password: passwordField
      ? Yup.string().required('Required').min(4, 'Too short')
      : undefined,
  })

  return (
    <Formik
      enableReinitialize
      initialValues={formInit}
      onSubmit={formSubmit}
      validationSchema={formValidate}
    >
      {({ touched, errors }) => (
        <Form>
          <div className='grid-2'>
            <FieldText
              label='First name'
              name='firstName'
              error={errors.firstName}
              touched={touched.firstName}
            />
            <FieldText
              label='Last name'
              name='lastName'
              error={errors.lastName}
              touched={touched.lastName}
            />
          </div>

          <div className='mt-5'>
            <FieldText
              type='email'
              label='Email address'
              name='email'
              error={errors.email}
              touched={touched.email}
            />
          </div>

          {passwordField && (
            <div className='mt-5'>
              <FieldText
                label='Password'
                name='password'
                error={errors.password}
                touched={touched.password}
              />
            </div>
          )}

          <div className='mt-5'>
            <FieldSelect
              label='Role'
              name='role'
              error={errors.role}
              touched={touched.role}
            >
              <option value='0'>Developer</option>
              <option value='1'>Admin</option>
            </FieldSelect>
          </div>

          <div className='mt-5 d-flex align-items-center'>
            <SubmitButton apiStatus={apiStatus} />
            <div className='ms-4'>
              <ApiStatus apiStatus={apiStatus} />
            </div>
          </div>
        </Form>
      )}
    </Formik>
  )
}
