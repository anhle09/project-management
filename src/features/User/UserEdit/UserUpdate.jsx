import { useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { USER_API_URL } from 'shared/constant/apiUrl'
import { useLocation, useNavigate } from 'react-router-dom'
import { userAction } from 'redux/Reducer/userSlice'
import useApi from 'shared/hook/api'
import UserForm from './UserForm'

export default function UserUpdate() {
  const { state } = useLocation()
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [response, putRequest] = useApi.patch(USER_API_URL, state.id)

  const handleModalHide = () => {
    navigate('/users')
  }

  const handleSubmit = async (formValues) => {
    const res = await putRequest(formValues)
    dispatch(userAction.queryUser())
    if (res) handleModalHide()
  }

  return (
    <Modal show onHide={handleModalHide} size='lg' centered>
      <Modal.Header closeButton>
        <Modal.Title>Update User</Modal.Title>
      </Modal.Header>

      <section className='p-6'>
        <UserForm
          formInit={state}
          formSubmit={handleSubmit}
          apiStatus={response.status}
        />
      </section>
    </Modal>
  )
}
