import { PencilAltIcon, TrashIcon } from '@heroicons/react/outline'
import { userAction } from 'redux/Reducer/userSlice'
import { useAuthDataSelector } from 'redux/store'
import { Badge } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { roles } from 'shared/constant/userRoles'
import userApi from 'shared/api/userApi'

export default function UserItem({ userData }) {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { currentUser } = useAuthDataSelector()

  const handleEdit = () => {
    navigate(`edit/${userData.id}`, { state: userData })
  }
  const handleDelete = async () => {
    await userApi.delete(userData.id)
    dispatch(userAction.queryUser())
  }

  return (
    <tr>
      <td>
        {userData.lastName} {userData.firstName}
      </td>
      <td>{userData.email}</td>
      <td>{roles[userData.role]}</td>
      <td>
        <Badge bg='success' className='text-white text-xs'>
          Active
        </Badge>
      </td>
      <td>
        <PencilAltIcon
          width={20}
          className='me-2 text-primary pointer'
          onClick={handleEdit}
        />
        {userData.email !== currentUser.email && (
          <TrashIcon
            width={20}
            className='text-danger pointer'
            onClick={handleDelete}
          />
        )}
      </td>
    </tr>
  )
}
