import { useUserDataSelector } from 'redux/store'
import { Link, Outlet } from 'react-router-dom'
import UserItem from './UserItem'

export default function UserList() {
  const { userList } = useUserDataSelector()

  return (
    <div className='px-6'>
      <Outlet />
      <div className='mb-5 d-flex align-items-center'>
        <h4 className='mb-0 me-5'>Project Users</h4>
        <Link to='create' className='btn btn-primary btn-sm text-white shadow'>
          Add new
        </Link>
      </div>
      {!userList.length && <p>User list is empty</p>}

      <div className='px-5 rounded shadow-sm bg-white overflow-auto'>
        <table className='table align-middle text-secondary mt-2 mb-0'>
          <thead>
            <tr className='text-sm'>
              <th>User Name</th>
              <th>Email Address</th>
              <th>Role</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {userList.map((item) => (
              <UserItem key={item.email} userData={item} />
            ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}
