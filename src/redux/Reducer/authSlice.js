import { createSlice } from '@reduxjs/toolkit'
import { removeStoredAuthToken } from 'shared/utils/authToken'

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    currentUser: { id: '', email: '', firstName: '', role: 0 },
    isStarting: true,
    isLoading: false,
    errorMessage: '',
  },
  reducers: {
    setAuthData: (state, action) => ({
      ...state,
      ...action.payload,
    }),
    getCurrentUser: (state) => {
      state.isStarting = true
    },
    login: (state) => {
      state.isLoading = true
    },
    register: (state) => {
      state.isLoading = true
    },
    changePassword: (state) => {
      state.isLoading = true
    },
    logout: (state) => {
      state.currentUser = {}
      removeStoredAuthToken()
    },
    resetError: (state) => {
      state.errorMessage = ''
    },
  },
})

export const authAction = authSlice.actions

const authReducer = authSlice.reducer

export default authReducer
