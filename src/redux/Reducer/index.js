import authReducer from './authSlice'
import userReducer from './userSlice'
import projectReducer from './projectSlice'
import taskReducer from './taskSlice'

const rootReducer = {
  auth: authReducer,
  user: userReducer,
  project: projectReducer,
  task: taskReducer,
}

export default rootReducer

export const selector = {
  authData: state => state.auth,
  userData: state => state.user,
  projectData: state => state.project,
  taskData: state => state.task,
}
