import { createSlice } from "@reduxjs/toolkit"

const projectSlice = createSlice({
  name: 'project',
  initialState: {
    projectList: [],
    isLoading: false,
    errorMessage: ''
  },
  reducers: {
    setProjectData: (state, action) => ({
      ...state, ...action.payload
    }),
    queryProject: (state, action) => {
      state.isLoading = true
    },
  }
})

export const projectAction = projectSlice.actions

const projectReducer = projectSlice.reducer

export default projectReducer
