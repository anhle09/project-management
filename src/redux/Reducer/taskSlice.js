import { createSlice } from "@reduxjs/toolkit"

const taskSlice = createSlice({
  name: 'task',
  initialState: {
    taskList: [],
    isLoading: false,
    errorMessage: ''
  },
  reducers: {
    setTaskData: (state, action) => ({
      ...state, ...action.payload
    }),
    queryTask: (state, action) => {
      state.isLoading = true
    },
  }
})

export const taskAction = taskSlice.actions

const taskReducer = taskSlice.reducer

export default taskReducer
