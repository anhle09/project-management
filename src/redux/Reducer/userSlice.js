import { createSlice } from "@reduxjs/toolkit"

const userSlice = createSlice({
  name: 'user',
  initialState: {
    userList: [],
    isLoading: false,
    errorMessage: ''
  },
  reducers: {
    setUserData: (state, action) => ({
      ...state, ...action.payload
    }),
    queryUser: (state, action) => {
      state.isLoading = true
    },
    createUser: (state, action) => {
      state.isLoading = true
    },
    updateUser: (state, action) => {
      state.isLoading = true
    },
    deleteUser: (state, action) => {
      state.isLoading = true
    }
  }
})

export const userAction = userSlice.actions

const userReducer = userSlice.reducer

export default userReducer
