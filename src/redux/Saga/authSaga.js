import { call, put, takeLatest } from 'redux-saga/effects'
import { authAction } from 'redux/Reducer/authSlice'
import { browserHistory } from 'app/Router'
import { DASHBOARD_PATH } from 'shared/constant/routerPath'
import { getStoredAuthToken, storeAuthToken } from 'shared/utils/authToken'
import authApi from 'shared/api/authApi'

function* handleRegister(action) {
  const response = yield call(authApi.register, action.payload)
  const payload = { isLoading: false }

  if (response) {
    const { user, token } = response
    payload.currentUser = user
    storeAuthToken(token)
    browserHistory.replace(DASHBOARD_PATH)
  } else {
    payload.errorMessage = 'Server error'
  }
  yield put(authAction.setAuthData(payload))
}

function* handleLogin(action) {
  const response = yield call(authApi.login, action.payload)
  const payload = { isLoading: false }

  if (response) {
    const { user, token } = response
    payload.currentUser = user
    storeAuthToken(token)
    browserHistory.replace(DASHBOARD_PATH)
  } else {
    payload.errorMessage = 'Email or password is invalid'
  }
  yield put(authAction.setAuthData(payload))
}

function* handleChangePassword(action) {
  const response = yield call(authApi.changePassword, action.payload)
  const payload = { isLoading: false }

  if (response) {
    browserHistory.replace(DASHBOARD_PATH)
  } else {
    payload.errorMessage = 'Password is invalid'
  }
  yield put(authAction.setAuthData(payload))
}

function* getCurrentUser() {
  const payload = { isStarting: false }
  const token = getStoredAuthToken()
  if (token) {
    const response = yield call(authApi.getCurrentUser)
    if (response) {
      payload.currentUser = response
    }
  }
  yield put(authAction.setAuthData(payload))
}

export default function* authSaga() {
  const registerType = authAction.register().type
  yield takeLatest(registerType, handleRegister)

  const loginType = authAction.login().type
  yield takeLatest(loginType, handleLogin)

  const changePwType = authAction.changePassword().type
  yield takeLatest(changePwType, handleChangePassword)

  const getUserType = authAction.getCurrentUser().type
  yield takeLatest(getUserType, getCurrentUser)
}
