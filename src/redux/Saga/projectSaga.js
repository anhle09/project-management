import { call, put, select, takeLatest } from 'redux-saga/effects'
import { projectAction } from 'redux/Reducer/projectSlice'
import { USER_ROLE } from 'shared/constant/userRoles'
import { selector } from 'redux/Reducer'
import projectApi from 'shared/api/projectApi'

function* handleQuery(action) {
  const { currentUser } = yield select(selector.authData)

  const projList = currentUser?.role === USER_ROLE
    ? yield call(projectApi.getByUser, { members: currentUser?.id })
    : yield call(projectApi.getAll)

  const payload = { isLoading: false }
  if (projList) payload.projectList = projList
  yield put(projectAction.setProjectData(payload))
}

export default function* projectSaga() {
  const queryType = projectAction.queryProject().type
  yield takeLatest(queryType, handleQuery)
}
