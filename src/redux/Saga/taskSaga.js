import { call, put, select, takeLatest } from 'redux-saga/effects'
import { taskAction } from 'redux/Reducer/taskSlice'
import { USER_ROLE } from 'shared/constant/userRoles'
import { selector } from 'redux/Reducer'
import taskApi from 'shared/api/taskApi'

function* handleQuery(action) {
  const { currentUser } = yield select(selector.authData)

  const taskList = currentUser?.role === USER_ROLE
    ? yield call(taskApi.getByUser, { member: currentUser?.id })
    : yield call(taskApi.getAll)

  const payload = { isLoading: false }
  if (taskList) payload.taskList = taskList
  yield put(taskAction.setTaskData(payload))
}

export default function* taskSaga() {
  const queryType = taskAction.queryTask().type
  yield takeLatest(queryType, handleQuery)
}
