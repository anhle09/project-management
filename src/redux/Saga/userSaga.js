import { call, put, takeLatest } from 'redux-saga/effects'
import { userAction } from 'redux/Reducer/userSlice'
import userApi from 'shared/api/userApi'

function* handleQueryUser(action) {
  const userList = yield call(userApi.getAll)
  const payload = { isLoading: false }
  if (userList) payload.userList = userList
  yield put(userAction.setUserData(payload))
}

export default function* userSaga() {
  const queryType = userAction.queryUser().type
  yield takeLatest(queryType, handleQueryUser)

  /* const createType = userAction.createUser().type
  yield takeLatest(createType, handleCreateUser)

  const updateType = userAction.updateUser().type
  yield takeLatest(updateType, handleUpdateUser)

  const deleteType = userAction.deleteUser().type
  yield takeLatest(deleteType, handleDeleteUser) */
}

/* 
function* handleApiResponse(response) {
  const payload = { isLoading: false }
  if (!response) {
    payload.errorMessage = 'Server error'
  }
  yield put(userAction.setUserData(payload))
}

function* handleCreateUser(action) {
  const res = yield call(userApi.create, action.payload)
  yield call(handleApiResponse, res)
}

function* handleUpdateUser(action) {
  const res = yield call(userApi.update, action.payload)
  yield call(handleApiResponse, res)
}

function* handleDeleteUser(action) {
  const res = yield call(userApi.delete, action.payload)
  yield call(handleApiResponse, res)
}
*/
