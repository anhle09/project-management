import { configureStore } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './Saga'
import rootReducer, { selector } from './Reducer'

const sagaMiddleware = createSagaMiddleware()

const store = configureStore({
  reducer: rootReducer,
  middleware: [sagaMiddleware],
})

sagaMiddleware.run(rootSaga)

export default store

export const useAuthDataSelector = () => useSelector(selector.authData)
export const useUserDataSelector = () => useSelector(selector.userData)
export const useProjectDataSelector = () => useSelector(selector.projectData)
export const useTaskDataSelector = () => useSelector(selector.taskData)
