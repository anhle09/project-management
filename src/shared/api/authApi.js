import api from './axiosApi'
import {
  CHANGE_PASSWORD_API_URL,
  CURRENT_USER_API_URL,
  LOGIN_API_URL,
  REGISTER_API_URL,
} from 'shared/constant/apiUrl'

const authApi = {
  register: async (payload) => (
    await api.post(REGISTER_API_URL, payload)
  ),
  login: async (payload) => (
    await api.post(LOGIN_API_URL, payload)
  ),
  changePassword: async (payload) => (
    await api.post(CHANGE_PASSWORD_API_URL, payload)
  ),
  getCurrentUser: async () => (
    await api.get(CURRENT_USER_API_URL)
  ),
}

export default authApi
