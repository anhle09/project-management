import axios from "axios"
import { API_URL } from 'shared/constant/apiUrl'
import { getStoredAuthToken } from 'shared/utils/authToken'

const api = axios.create({
  baseURL: API_URL,
  headers: { 'Content-Type': 'application/json' },
})

api.interceptors.request.use(config => {
  const token = getStoredAuthToken()
  if (token) {
    config.headers.Authorization = 'Bearer ' + token
  }
  return config
})

api.interceptors.response.use(
  res => res.data.data,
  err => null
)

export const apiRequest = (method, url, data) => {
  const options = { method, url, data }
  return api(options)
}

export default api
