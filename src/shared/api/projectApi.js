import api from './axiosApi'
import { PROJECT_API_URL } from 'shared/constant/apiUrl'

const projectApi = {
  getAll: async () => (
    await api.get(PROJECT_API_URL)
  ),
  getByUser: async (params) => (
    await api.get(PROJECT_API_URL, { params })
  ),
  update: async (id, payload) => (
    await api.patch(PROJECT_API_URL + '/' + id, payload)
  ),
  delete: async (id) => (
    await api.delete(PROJECT_API_URL + '/' + id)
  ),
}

export default projectApi
