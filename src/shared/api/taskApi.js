import api from './axiosApi'
import { TASK_API_URL } from 'shared/constant/apiUrl'

const taskApi = {
  getAll: async () => (
    await api.get(TASK_API_URL)
  ),
  getByUser: async (params) => (
    await api.get(TASK_API_URL, { params })
  ),
  delete: async (id) => (
    await api.delete(TASK_API_URL + '/' + id)
  ),
}

export default taskApi
