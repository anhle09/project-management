import { USER_API_URL } from 'shared/constant/apiUrl'
import api from './axiosApi'

const userApi = {
  getAll: async () => (
    await api.get(USER_API_URL)
  ),
  query: async (params) => (
    await api.get(USER_API_URL, { params })
  ),
  create: async (payload) => (
    await api.post(USER_API_URL, payload)
  ),
  update: async (id, payload) => (
    await api.patch(USER_API_URL + '/' + id, payload)
  ),
  delete: async (id) => (
    await api.delete(USER_API_URL + '/' + id)
  ),
}

export default userApi
