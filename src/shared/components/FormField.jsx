import { Field } from 'formik'
import { Button } from 'react-bootstrap'
import LoadingSpinner from './LoadingSpinner'

export const FieldText = ({ type = 'text', label, name, error, touched }) => (
  <label className='d-block'>
    <h6 className='mb-3'>{label}</h6>
    <Field
      name={name}
      type={type}
      className={touched && error ? 'form-control form-error' : 'form-control'}
    />
    {touched && error && <small className='mt-1 text-danger'>{error}</small>}
  </label>
)

export const FieldTextarea = ({ label, name, error, touched, rows = 6 }) => (
  <label className='d-block'>
    <h6 className='mb-3'>{label}</h6>
    <Field
      name={name}
      as='textarea'
      rows={rows}
      className={touched && error ? 'form-control form-error' : 'form-control'}
    />
    {touched && error && <small className='mt-1 text-danger'>{error}</small>}
  </label>
)

export const FieldSelect = ({ label, name, children, error, touched }) => (
  <label className='d-block'>
    <h6 className='mb-3'>{label}</h6>
    <Field
      name={name}
      as='select'
      className={touched && error ? 'form-select form-error' : 'form-select'}
    >
      <option value=''>Select</option>
      {children}
    </Field>
    {touched && error && <small className='mt-1 text-danger'>{error}</small>}
  </label>
)

export const FieldCheckbox = ({ label, name, value }) => (
  <label className='d-flex align-items-center'>
    <Field
      name={name}
      type='checkbox'
      value={value}
      className='form-check-input'
    />
    <small className='ms-2'>{label}</small>
  </label>
)

/* date format: yyyy/mm/dd */
export const FieldDate = ({ label, name, error, touched }) => (
  <label className='d-block'>
    <h6 className='mb-3'>{label}</h6>
    <Field
      name={name}
      type='date'
      /* defaultValue={new Date().toISOString().slice(0, 10)} */
      className={touched && error ? 'form-control form-error' : 'form-control'}
    />
    {touched && error && <small className='text-danger'>{error}</small>}
  </label>
)

export const SubmitButton = ({ apiStatus }) => (
  <Button variant='success' type='submit' className='text-white'>
    Submit <LoadingSpinner active={apiStatus.loading} />
  </Button>
)

export const ApiStatus = ({ apiStatus }) => {
  if (apiStatus.successful)
    return <small className='text-success'>Submit successfully</small>
  else if (apiStatus.failed)
    return <small className='text-danger'>Submit error</small>
}
