import { Outlet } from 'react-router-dom'
import Sidebar from 'shared/components/Sidebar'
import Topbar from 'shared/components/Topbar'
import classes from './styles.module.scss'

export default function MainLayout() {
  return (
    <>
      <Topbar />
      <Sidebar />
      <main className={classes.container}>
        <Outlet />
      </main>
    </>
  )
}
