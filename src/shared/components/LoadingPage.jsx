import { Spinner } from 'react-bootstrap'

export default function LoadingPage() {
  return (
    <div className='d-flex vh-100 justify-content-center align-items-center'>
      <Spinner animation='border' variant='secondary' />
    </div>
  )
}
