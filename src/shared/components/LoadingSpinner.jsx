import { Spinner } from 'react-bootstrap'

export default function LoadingSpinner({ active }) {
  if (active) {
    return <Spinner animation='border' variant='light' size='sm' />
  }
  return null
}
