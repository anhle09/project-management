export default function NotFoundPage() {
  return <h3 className='mt-8 text-center'>404 | Not Found</h3>
}
