import { useAuthDataSelector } from 'redux/store'
import { ADMIN_ROLE, USER_ROLE } from 'shared/constant/userRoles'

export function AdminRoleRequired({ children }) {
  const { currentUser } = useAuthDataSelector()

  if (currentUser.role === ADMIN_ROLE) {
    return children
  }
  return null
}

export function UserRoleRequired({ children }) {
  const { currentUser } = useAuthDataSelector()

  if (currentUser.role === USER_ROLE) {
    return children
  }
  return null
}
