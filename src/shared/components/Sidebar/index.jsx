import { Link, useMatch } from 'react-router-dom'
import { AdminRoleRequired } from 'shared/components/ProtectedRole'
import { LOGO_URL } from 'shared/constant/assetUrl'
import classes from './styles.module.scss'
import {
  ArchiveIcon,
  HomeIcon,
  ServerIcon,
  UserGroupIcon,
} from '@heroicons/react/outline'

export default function Sidebar() {
  return (
    <aside className={classes.container}>
      <Link to='/' className={classes.logo}>
        <img src={LOGO_URL} alt='logo' />
      </Link>

      <NavItem path='/dashboard'>
        <HomeIcon />
        <span>Dashboard</span>
      </NavItem>

      <NavItem path='/projects'>
        <ServerIcon />
        <span>Projects</span>
      </NavItem>

      <NavItem path='/tasks'>
        <ArchiveIcon />
        <span>Tasks</span>
      </NavItem>

      <AdminRoleRequired>
        <NavItem path='/users'>
          <UserGroupIcon />
          <span>Users</span>
        </NavItem>
      </AdminRoleRequired>
    </aside>
  )
}

function NavItem({ path, children }) {
  const matched = useMatch(path + '/*')

  return (
    <Link
      to={path}
      className={classes.nav_item}
      style={{ color: !!matched ? 'white' : '' }}
    >
      {children}
    </Link>
  )
}
