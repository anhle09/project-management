import classes from './styles.module.scss'

export function WarningLabel({ children }) {
  return <span className={classes.label_warning}>{children}</span>
}

export function TaskStatusLabel({ status }) {
  switch (status) {
    case 0:
      return <span className={classes.label_danger}>Pending</span>
    case 1:
      return <span className={classes.label_warning}>In-progress</span>
    default:
      return <span className={classes.label_success}>Completed</span>
  }
}

export function ProjectPriorityLabel({ priority }) {
  switch (priority) {
    case 0:
      return <span className={classes.label_warning}>Medium</span>
    default:
      return <span className={classes.label_danger}>High</span>
  }
}
