import { Link } from 'react-router-dom'
import { useAuthDataSelector } from 'redux/store'
import { ADMIN_ROLE } from 'shared/constant/userRoles'

export default function TaskItemLink({ id, children }) {
  const { currentUser } = useAuthDataSelector()
  if (currentUser.role === ADMIN_ROLE) {
    return <Link to={`/tasks/${id}`}>{children}</Link>
  } else {
    return <Link to='/tasks'>{children}</Link>
  }
}
