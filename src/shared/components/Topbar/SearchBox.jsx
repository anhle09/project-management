import { useState } from 'react'
import { Link } from 'react-router-dom'
import { Dropdown } from 'react-bootstrap'
import { SearchIcon } from '@heroicons/react/outline'
import { useProjectDataSelector, useTaskDataSelector } from 'redux/store'
import classes from './styles.module.scss'

export function SearchBox() {
  const { projectList } = useProjectDataSelector()
  const { taskList } = useTaskDataSelector()

  const [searchResult, setSearchResult] = useState(null)
  const [menuShow, setMenuShow] = useState(false)
  const [inputValue, setInputValue] = useState('')

  const toggleMenuShow = () => {
    setMenuShow(false)
    setInputValue('')
  }

  const handleSearch = (event) => {
    const inputText = event.target.value
    if (!inputText.length) toggleMenuShow()
    const text = inputText?.toLowerCase().trim()
    if (text < 1) return
    setInputValue(inputText?.trim())

    const project = projectList.filter((item) => {
      const name = item.name?.toLowerCase()
      return name.includes(text)
    })
    const task = taskList.filter((item) => {
      const name = item.name?.toLowerCase()
      return name.includes(text)
    })

    if (project.length || task.length) {
      setSearchResult({ project, task })
    } else {
      setSearchResult(null)
    }
    if (!menuShow) setMenuShow(true)
  }

  const projSearchList = searchResult?.project
  const taskSearchList = searchResult?.task

  return (
    <div className='me-4'>
      <div className={classes.search_box}>
        <span>
          <SearchIcon width={20} color='gray' />
        </span>
        <input
          className='form-control'
          placeholder='Search'
          value={inputValue}
          onChange={handleSearch}
        />
      </div>

      <Dropdown show={menuShow} onToggle={toggleMenuShow}>
        <Dropdown.Menu className='w-100 mt-2 shadow'>
          <div className={classes.search_result}>
            <ProjectSearchResult projList={projSearchList} />
            {!!projSearchList?.length && !!taskSearchList?.length && (
              <Dropdown.Divider />
            )}
            <TaskSearchResult taskList={taskSearchList} />
            {!searchResult && <Dropdown.Item as='div'>No result</Dropdown.Item>}
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  )
}

function ProjectSearchResult({ projList }) {
  if (projList?.length)
    return (
      <>
        <Dropdown.Header>Project</Dropdown.Header>
        {projList.map((item) => (
          <Dropdown.Item as='div' key={item.id}>
            <Link to={`/projects/detail/${item.id}`} className='d-block'>
              {item.name}
            </Link>
          </Dropdown.Item>
        ))}
      </>
    )
  return null
}

function TaskSearchResult({ taskList }) {
  if (taskList?.length)
    return (
      <>
        <Dropdown.Header>Task</Dropdown.Header>
        {taskList.map((item) => (
          <Dropdown.Item as='div' key={item.id}>
            <Link to={`/tasks/${item.id}`}>{item.name}</Link>
          </Dropdown.Item>
        ))}
      </>
    )
  return null
}
