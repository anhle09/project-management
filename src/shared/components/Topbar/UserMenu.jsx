import { useDispatch } from 'react-redux'
import { Button, Dropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { authAction } from 'redux/Reducer/authSlice'
import { useAuthDataSelector } from 'redux/store'
import { UserCircleIcon } from '@heroicons/react/solid'

export default function UserMenu() {
  const dispatch = useDispatch()
  const { currentUser } = useAuthDataSelector()

  const handleLogout = async () => {
    dispatch(authAction.logout())
  }

  return (
    <Dropdown>
      <Dropdown.Toggle as='div'>
        <UserCircleIcon width={40} color='gray' />
      </Dropdown.Toggle>

      <Dropdown.Menu className='mt-2 text-center shadow-sm'>
        <div style={{ width: 200 }}>
          <div className='px-6 py-4'>
            <UserCircleIcon width={60} color='gray' />
            <h6 className='my-1'>{currentUser.firstName}</h6>
            <small className='mb-1'>{currentUser.email}</small>
            <Link
              to='change-password'
              className='text-xs text-decoration-underline'
            >
              Change password
            </Link>
          </div>
          <Dropdown.Divider className='m-0' />

          <Button
            size='sm'
            className='mt-3 mb-1 px-6 text-white rounded-pill'
            onClick={handleLogout}
          >
            Log out
          </Button>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  )
}
