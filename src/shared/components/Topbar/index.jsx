import { BellIcon } from '@heroicons/react/outline'
import { SearchBox } from './SearchBox'
import UserMenu from './UserMenu'
import classes from './styles.module.scss'

export default function Topbar() {
  return (
    <header className={classes.topbar}>
      <section>
        <SearchBox />
        <BellIcon width={24} color='gray' className='mx-5 not-allowed' />
        <UserMenu />
      </section>
    </header>
  )
}
