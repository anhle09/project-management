export const HOST_URL = process.env.REACT_APP_HOST_URL || 'http://localhost:5000'
export const API_URL = HOST_URL + '/api'

export const USER_API_URL = '/users'
export const PROJECT_API_URL = '/projects'
export const TASK_API_URL = '/tasks'

export const CURRENT_USER_API_URL = '/users/currentUser'
export const REGISTER_API_URL = '/auth/register'
export const LOGIN_API_URL = '/auth/login'
export const CHANGE_PASSWORD_API_URL = '/auth/change-password'
