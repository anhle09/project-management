export const projectStatus = ['Open', 'Closed']
export const PROJECT_OPEN = 0
export const PROJECT_CLOSED = 1

export const TASK_PENDING = 0
export const TASK_IN_PROGRESS = 1
export const TASK_COMPLETED = 2

export const HIGH_PRIORITY = 1
export const MEDIUM_PRIORITY = 0