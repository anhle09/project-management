import { TASK_COMPLETED, TASK_IN_PROGRESS, TASK_PENDING } from './projectStatus'
import { ADMIN_ROLE, USER_ROLE } from './userRoles'

const testEnv = process.env.NODE_ENV === 'test'
const testRole = process.env.REACT_APP_USER_ROLE

const userTestData = {
  id: 'USER_ID',
  email: 'USER_EMAIL',
  firstName: 'USER_NAME',
  role: testRole === 'admin' ? ADMIN_ROLE : USER_ROLE,
}

const taskTestData = (taskStatus = 0) => ({
  id: 'TASK_ID',
  project: 'PROJECT_ID',
  member: 'USER_ID',
  name: 'TASK_NAME',
  description: 'TASK_DESCRIPTION',
  startDate: '2022-01-11',
  endDate: '2022-01-01',
  status: taskStatus,
})

const taskList = [taskTestData(0), taskTestData(1), taskTestData(2)]

export const initUser = testEnv ? userTestData : {}
export const initTaskList = testEnv ? taskList : []
export const taskPending = taskTestData(TASK_PENDING)
export const taskInprogress = taskTestData(TASK_IN_PROGRESS)
export const taskCompleted = taskTestData(TASK_COMPLETED)
