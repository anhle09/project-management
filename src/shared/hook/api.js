/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect } from 'react'
import api, { apiRequest } from 'shared/api/axiosApi'

const apiStatus = {
  idle: false,
  loading: false,
  successful: false,
  failed: false,
}

function useQuery(url, params) {
  const [data, setData] = useState(null)
  const [status, setStatus] = useState(apiStatus)

  useEffect(() => {
    const getRequest = async () => {
      setStatus({ ...apiStatus, loading: true })
      const data = await api.get(url, { params })
      if (data) {
        setStatus({ ...apiStatus, successful: true })
        setData(data)
      }
      else {
        setStatus({ ...apiStatus, failed: true })
      }
    }
    getRequest()
  }, [])

  return { status, data }
}

function useMutation(method, url) {
  const [data, setData] = useState(null)
  const [status, setStatus] = useState(apiStatus)

  const mutationRequest = async (body) => {
    setStatus({ ...apiStatus, loading: true })
    const data = await apiRequest(method, url, body)
    if (data) {
      setStatus({ ...apiStatus, successful: true })
      setData(data)
    }
    else {
      setStatus({ ...apiStatus, failed: true })
    }
    return data
  }

  const response = { status, data }
  return [response, mutationRequest]
}

const useApi = {
  get: (url, params) => useQuery(url, params),
  post: (url) => useMutation('POST', url),
  patch: (url, id) => useMutation('PATCH', url + '/' + id),
  put: (url, id) => useMutation('PUT', url + '/' + id),
  delete: (url, id) => useMutation('DELETE', url + '/' + id),
}

export default useApi
