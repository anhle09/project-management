export const getStoredAuthToken = () => {
  return localStorage.getItem('managementApp_authToken')
}

export const storeAuthToken = (token) => {
  localStorage.setItem('managementApp_authToken', token)
}

export const removeStoredAuthToken = () => {
  localStorage.removeItem('managementApp_authToken')
}
