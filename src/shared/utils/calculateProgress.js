import moment from 'moment'

export default function calculateProgress(startDate, endDate) {
  if (!startDate || !endDate) return 0
  const start = moment(startDate)
  const end = moment(endDate)
  const now = moment()
  if (start.isAfter(now)) return 0
  if (end.isBefore(now)) return 100

  const totalDate = end.diff(start, 'days')
  const fromNow = now.diff(start, 'days')
  const progress = Math.round((fromNow / totalDate) * 100)
  return progress
}