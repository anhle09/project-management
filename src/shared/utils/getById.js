export default function getById(dataList, id) {
  if (dataList?.length) {
    return dataList.filter(item => item.id === id)[0]
  }
  return null
}
