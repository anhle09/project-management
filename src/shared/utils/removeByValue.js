export default function removeByValue(stringArray, value) {
  if (stringArray?.length) {
    const newArray = [...stringArray]
    const index = newArray.indexOf(value)
    newArray.splice(index, 1)
    return newArray
  }
}